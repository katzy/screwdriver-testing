#!/bin/bash -e

if [ -z "$GCLOUD_PRIVATE_KEY" ] || [ -z "$GCLOUD_PRIVATE_KEY_ID" ]; then
  echo Unable to setup gcloud cli, missing environment variables
  exit 2
fi

echo Downloading gcloud
curl -O -J https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/${SDK_FILENAME}
echo Untaring gcloud
tar -zxvf ${SDK_FILENAME} --directory . > /dev/null 2>&1
export PATH=${PATH}:./google-cloud-sdk/bin
ls -la ./google-cloud-sdk/bin
echo Setting up secrets
python bin/createSecret.py > client-secret.json
echo Authenticating gcloud cli
gcloud auth activate-service-account --key-file client-secret.json
gcloud components install alpha
rm client-secret.json
