#!/bin/bash -e
# K8S_HOST = Kubernetes Hostname
# K8S_TOKEN = Kubernetes Service Account Token
# K8S_DEPLOYMENT = Kubernetes Deployment Name
# K8S_IMAGE = Docker image name (without the version)
if [ -z "$K8S_TOKEN" ]  || [ -z "$K8S_DEPLOYMENT" ] || [ -z "$K8S_IMAGE" ] || [ -z "$K8S_HOST" ]; then
  echo Unable to kubernetes trigger, missing environment variables
  exit 2
fi

K8S_TAG=`meta get tags.${CHANGE_DIR}`

if [ -z "$NEEDS_PUBLISH" ] || [ $NEEDS_PUBLISH = null ]; then
  echo "No need to deploy. No change in directory"
  echo $NEEDS_PUBLISH
  exit 0
fi

echo "Triggering Kubernetes deployment"
URL=https://${K8S_HOST}/apis/extensions/v1beta1/namespaces/default/deployments/${K8S_DEPLOYMENT}
BODY="{\"spec\":{\"template\":{\"spec\":{\"containers\":[{\"name\":\"${K8S_DEPLOYMENT}\",\"image\":\"${K8S_IMAGE}:${K8S_TAG}\"}]}}}}"
curl -k -i \
  -XPATCH \
  -H "Accept: application/json, */*" \
  -H "Authorization: Bearer ${K8S_TOKEN}" \
  -H "Content-Type: application/strategic-merge-patch+json" \
  -d $BODY \
  $URL > /tmp/k8s_out
grep "200 OK" /tmp/k8s_out || (echo "Failed deployment" && cat /tmp/k8s_out && exit 1)
