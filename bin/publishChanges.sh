#!/bin/bash -e
NEEDS_PUBLISH=`meta get "dirs.react_web"`

if [ -z "$NEEDS_PUBLISH" ] || [ $NEEDS_PUBLISH = null ]; then
  echo "No need to publish"
  echo $NEEDS_PUBLISH
  exit 0
fi

echo Packaging up public dir
tar -C public -cvzf $RELEASE_FILE .
echo Uploading to google storage
gsutil cp $RELEASE_FILE gs://curbside-frontend/latest
echo Making google storage public
gsutil acl ch -u AllUsers:R gs://curbside-frontend/latest
