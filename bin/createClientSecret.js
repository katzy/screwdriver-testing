'use strict';
const fs = require('fs');
const filename = './client-secret.json';
const privKey=process.env.GCLOUD_PRIVATE_KEY;
const privKeyId=process.env.GCLOUD_PRIVATE_KEY_ID;
const secrets = {
  "type": "service_account",
  "project_id": "core-yew-111115",
  "private_key_id": "",
  "private_key": "",
  "client_email": "screwdriver@core-yew-111115.iam.gserviceaccount.com",
  "client_id": "104392935383262484273",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://accounts.google.com/o/oauth2/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/screwdriver%40core-yew-111115.iam.gserviceaccount.com"
};

secrets["private_key"] = privKey;
secrets["private_key_id"] = privKeyId;

fs.writeFileSync(filename, JSON.stringify(secrets));
