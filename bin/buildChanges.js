'use strict';
const execFile = require('child_process').execFile;
const path = require('path');

function getDirChanges() {
    return new Promise((resolve, reject) => {
        execFile('meta', ['get', 'dirs'], (error, stdout, stderr) => {
            if (error) {
                console.log(error);

                return resolve([]);
            }

            const foo = JSON.parse(stdout);
            console.log(foo);

            return resolve(Object.keys(foo));
        });
    });
}

function runNpmBuild(cwd) {
    if (cwd !== 'react_web') {
        console.log(`No need to run build for dir ${cwd}`);

        return Promise.resolve();
    }

    return new Promise((resolve, reject) => {
        execFile('npm', ['install'], { cwd: 'react-web' } , (error, stdo) => {
            if (error) {
                return reject(error);
            }

            execFile('npm', ['run', 'build'], {
                cwd
            }, (err, stdout) => {
                console.log(`Running 'npm test' for dir "${cwd}"`);
                console.log(stdout);

                if (err) {
                    return reject(err);
                }

                return resolve();
            });
        });
    });
}

function runBuilds(dirs) {
    return Promise.all(
        dirs.map(dir => runNpmBuild(dir))
    );
}

getDirChanges()
    .then(dirs => runBuilds(dirs))
    .catch((err) => {
        console.log('Failed tests', err);
        process.exit(1);
    });

// getCommit()
//     .then(commit => getChangedDirectories(commit));
