'use strict';
const cp = require('child_process')
const execFile = cp.execFile;
const spawn = cp.spawn;
const path = require('path');

function getMetaDirs() {
    return new Promise((resolve, reject) => {
        execFile('meta', ['get', 'dirs'], (error, stdout, stderr) => {
            if (error) {
                console.log(error);

                return resolve(['./']);
            }

            const foo = JSON.parse(stdout);
            console.log(foo);

            return resolve(Object.keys(foo));
        });
    });
}

function runNpmInstall(cwd) {
    if (cwd === 'base') {
        cwd = './'
    }

    if (cwd === 'react_web') {
        cwd = 'react-web';
    }

    return new Promise((resolve, reject) => {
        const install = spawn('npm', ['install'], { cwd });

        install.stdout.on('data', (data) => {
            console.log(data);
        });

        install.stderr.on('data', (data) => {
            console.log(`stderr: ${data}`);
        });

        install.on('close', (code) => {
            if (code !== 0) {
                return reject(code);
            }

            return resolve(code);
        });
    });
}

function runNpmTest(cwd) {
    if (cwd === 'base') {
        cwd = './'
    }

    if (cwd === 'react_web') {
        cwd = 'react-web';
    }

    return new Promise((resolve, reject) => {
        execFile('npm', ['test'], {
            cwd
        }, (err, stdout) => {
            console.log(`Running 'npm test' for dir "${cwd}"`);
            console.log(stdout);

            if (err) {
                return reject(err);
            }

            return resolve();
        });
    });
}

function runTests(dirs) {
    console.log('Running tests for', dirs);

    return Promise.all(
        dirs.map(dir => runNpmInstall(dir)
            .then(() => runNpmTest(dir)))
    );
}

getMetaDirs()
    .then(dirs => runTests(dirs))
    .catch((err) => {
        console.log('Failed tests', err);
        process.exit(1);
    });

// getCommit()
//     .then(commit => getChangedDirectories(commit));
