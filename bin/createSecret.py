import sys, json
import os

secret={
  "type": "service_account",
  "project_id": "core-yew-111115",
  "private_key_id": "75b523ae3b919b2162863ddf1bee1e3ec4dd82f6",
  "private_key": "",
  "client_email": "screwdriver@core-yew-111115.iam.gserviceaccount.com",
  "client_id": "104392935383262484273",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://accounts.google.com/o/oauth2/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/screwdriver%40core-yew-111115.iam.gserviceaccount.com"
}

secret["private_key"] = os.environ['GCLOUD_PRIVATE_KEY'].replace('\\n', '\n')

print(json.dumps(secret))
