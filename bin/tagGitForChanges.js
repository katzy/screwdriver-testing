'use strict';
const execFile = require('child_process').execFile;
const path = require('path');

function getMetaDirs() {
    return new Promise((resolve, reject) => {
        execFile('meta', ['get', 'dirs'], (error, stdout, stderr) => {
            if (error) {
                console.log(error);

                return resolve(null);
            }

            const foo = JSON.parse(stdout);
            console.log(foo);

            return resolve(Object.keys(foo));
        });
    });
}

function tagMeta(dir, hash) {
    const tag = `${dir}-${hash}`;

    execFile('meta', ['set', `tags.${dir}`, tag], (error, stdout, stderr) => {
        if (error) {
            console.log(error);
        }

        console.log(stdout);
    });
}

function getGitHash() {
    return new Promise((resolve, reject) => {
        execFile('git', ['log', '-1', '--pretty=format:%h'], (err, stdout) => {
            if (err) {
                return reject(err);
            }

            return resolve(stdout.trim());
        })
    })
}

function pushGitTags() {
    return new Promise((resolve, reject) => {
        execFile('git', ['push', 'origin', '--tags'], (err, stdout) => {
            if (err) {
                console.log(err);
            }

            console.log(stdout);

            return resolve();
        });
    });
}

function tagGitDir(dir, hash) {
    const tag = `${dir}-${hash}`;
    return new Promise((resolve, reject) => {
        execFile('git', ['tag', `${dir}-${hash}`], (err, stdout) => {
            if (err) {
                console.log(err);
            }

            console.log(stdout);

            return resolve(tag);
        });
    });
}

function tagGitDirs(dirs, hash) {
    return Promise.all(
        dirs.map(dir => tagGitDir(dir, hash))
    );
}

Promise.all([
    getMetaDirs(['bin', './']),
    getGitHash()
]).then(rs =>
    tagGitDirs(rs[0], rs[1])
        .then(() => tagMeta(rs[0], rs[1]))
)
.then(() => pushGitTags());

// getCommit()
//     .then(commit => getChangedDirectories(commit));
