#!/bin/bash -e
# CHANGE_DIR = The meta position to look for tag in
# K8S_IMAGE = The docker image to wait for
if [ -z "$CHANGE_DIR" ] || [ -z "$K8S_IMAGE" ]; then
  echo Unable to docker wait, missing environment variables
  exit 2
fi

DOCKER_TAG=`meta get tags.${CHANGE_DIR}`
NEEDS_PUBLISH=`meta get dirs.${CHANGE_DIR}`

if [ -z "$NEEDS_PUBLISH" ] || [ $NEEDS_PUBLISH = null ]; then
  echo "No need to wait. No change in directory"
  echo $NEEDS_PUBLISH
  exit 0
fi

function check_status {
  gcloud alpha container images describe $K8S_IMAGE:$DOCKER_TAG
}

echo Looking for image $K8S_IMAGE:$DOCKER_TAG

MINUTES=0
until check_status
do
  if [ $MINUTES -gt 20 ] ; then
    echo "Timed out after 20 minutes"
    exit 1
  fi

  echo "Not available yet ($MINUTES minutes elapsed)"
  sleep 60
  ((MINUTES+=1))
done
echo Image found
