'use strict';
const execFile = require('child_process').execFile;
const path = require('path');

function metaSetDir(dir) {
    if (dir === 'react-web') {
        dir = 'react_web';
    }

    return new Promise((resolve, reject) => {
        execFile('meta', ['set', `dirs.${dir}`, true], (error, stdout, stderr) => {
            if (error) {
                console.log(error);
            }

            console.log(stdout);
        });
    });
}

function metaSetDirs(dirs) {
    return Promise.all(
        dirs.map(dir => metaSetDir(dir))
    );
}

function getChangedDirectories() {
    const changedDirs = {};
    return new Promise((resolve, reject) => {
        execFile('git', ['show', '--stat'], (error, stdout, stderr) => {
            if (error) {
                return reject(error);
            }

            const lines = stdout.split('\n').filter(s => !!s).map(s => s.trim()).reverse();
            const numChanges = parseInt(lines[0].split(' ')[0]);

            for (let i = 1; i <= numChanges; i++) {
                const fname = lines[i].split(' ')[0];
                const dirs = fname.split('/');
                const baseDir = dirs[0];

                console.log(dirs);

                if (dirs.length === 1) {
                    changedDirs['base'] = true;
                } else {
                    changedDirs[baseDir] = true;
                }
            }
            console.log(changedDirs);

            return resolve(Object.keys(changedDirs));
        });
    });
}

getChangedDirectories()
    .then(dirs => metaSetDirs(dirs))
    .catch(err => {
        console.log('Failed to set changed dirs', err);
        process.exit(1);
    })

// getCommit()
//     .then(commit => getChangedDirectories(commit));
